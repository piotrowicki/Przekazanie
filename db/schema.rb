# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140912065648) do

  create_table "companies", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "equipment", force: true do |t|
    t.string   "name"
    t.string   "mark"
    t.string   "serial"
    t.string   "quantity"
    t.string   "brutto"
    t.string   "v_brutto"
    t.string   "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "protocol_id"
  end

  add_index "equipment", ["protocol_id"], name: "index_equipment_on_protocol_id", using: :btree

  create_table "protocols", force: true do |t|
    t.datetime "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "number"
    t.integer  "user_to_id"
    t.integer  "company_id"
    t.integer  "company_to_id"
  end

  add_index "protocols", ["company_id"], name: "index_protocols_on_company_id", using: :btree
  add_index "protocols", ["company_to_id"], name: "index_protocols_on_company_to_id", using: :btree
  add_index "protocols", ["user_id"], name: "index_protocols_on_user_id", using: :btree
  add_index "protocols", ["user_to_id"], name: "index_protocols_on_user_to_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id"
    t.integer  "privilege"
  end

  add_index "users", ["company_id"], name: "index_users_on_company_id", using: :btree

end
