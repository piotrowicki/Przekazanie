class AddUserToProtocol < ActiveRecord::Migration
  def change
    add_reference :protocols, :user, index: true
  end
end
