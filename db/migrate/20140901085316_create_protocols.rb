class CreateProtocols < ActiveRecord::Migration
  def change
    create_table :protocols do |t|
      t.date :date

      t.timestamps
    end
  end
end
