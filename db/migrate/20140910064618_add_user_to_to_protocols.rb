class AddUserToToProtocols < ActiveRecord::Migration
  def change
    add_reference :protocols, :user_to, index: true
  end
end
