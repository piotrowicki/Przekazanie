class CreateEquipment < ActiveRecord::Migration
  def change
    create_table :equipment do |t|
      t.string :name
      t.string :mark
      t.string :serial
      t.string :quantity
      t.string :brutto
      t.string :v_brutto
      t.string :comments

      t.timestamps
    end
  end
end
