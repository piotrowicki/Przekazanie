class AddCompanyToToProtocols < ActiveRecord::Migration
  def change
    add_reference :protocols, :company_to, index: true
  end
end
