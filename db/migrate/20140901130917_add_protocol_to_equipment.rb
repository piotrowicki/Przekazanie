class AddProtocolToEquipment < ActiveRecord::Migration
  def change
    add_reference :equipment, :protocol, index: true
  end
end
