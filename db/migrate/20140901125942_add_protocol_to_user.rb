class AddProtocolToUser < ActiveRecord::Migration
  def change
    add_reference :users, :protocol, index: true
  end
end
