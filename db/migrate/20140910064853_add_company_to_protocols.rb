class AddCompanyToProtocols < ActiveRecord::Migration
  def change
    add_reference :protocols, :company, index: true
  end
end
