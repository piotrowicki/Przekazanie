class ProtocolsController < ApplicationController
  before_action :set_protocol, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate, except: [:index, :show]
  
  helper_method :generate
  # GET /protocols
  # GET /protocols.json
  
  def index
    @protocols = Protocol.order(:number)
   
  end

  # GET /protocols/1
  # GET /protocols/1.json
  def show
    
    @equip = Protocol.find(params[:id]).equipments
  end
  
  def generate 
    require 'spreadsheet'
    @equip = Protocol.find(params[:id])
    
   
        
    #spreadsheet = StringIO.new 
    #    book = Spreadsheet::Workbook.new
    #    sheet1 = book.create_worksheet
    #    sheet1.name = 'My First Worksheet'
    #    sheet1[1,0] = 'Japan'
            
    book = Spreadsheet.open 'public/xls/test.xls'
    sheet = book.worksheet 0
            
    buffer = StringIO.new
    book.write(buffer)
    buffer.rewind
    
    
    
    
    book.write 'public/xls/data.xls'
    
  
    send_file 'public/xls/data.xls', :filename => @equip.number, :type =>  "application/vnd.ms-excel", :stream => false
    
    #    send_data spreadsheet.string, :filename => "yourfile.xls", :type =>  "application/vnd.ms-excel"

    
  end
  

  
  

  # GET /protocols/new
  def new
    @protocol = Protocol.new
    
    #linia dodajaca nowy equipment na starcie
    @protocol.equipments.build
    
  end

  # GET /protocols/1/edit
  def edit
  end
  
  # POST /protocols
  # POST /protocols.json
  def create
    @protocol = Protocol.new(protocol_params)
   
    
    respond_to do |format|
      if @protocol.save
        format.html { redirect_to @protocol, notice: 'Protocol was successfully created.' }
        format.json { render :show, status: :created, location: @protocol }
      else
        format.html { render :new }
        format.json { render json: @protocol.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /protocols/1
  # PATCH/PUT /protocols/1.json
  def update
 
    respond_to do |format|
      if @protocol.update(protocol_params)
        format.html { redirect_to @protocol, notice: 'Protocol was successfully updated.' }
        format.json { render :show, status: :ok, location: @protocol }
      else
        format.html { render :edit }
        format.json { render json: @protocol.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /protocols/1
  # DELETE /protocols/1.json
  def destroy
    @protocol.destroy
    respond_to do |format|
      format.html { redirect_to protocols_url, notice: 'Protocol was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_protocol
    @protocol = Protocol.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def protocol_params
    params.require(:protocol).permit(:date, :user_id, :number, :user_to_id, :company_id, :company_to_id, equipments_attributes: [:id, :name, :mark, :serial, :quantity, :brutto, :v_brutto, :comments, :_destroy])
  end
end
