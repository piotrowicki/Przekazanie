class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  helper_method :current_user

  private
  
  #Autentykacja
  def authenticate
    #authenticate_or_request_with_http_basic do |username, password|
    # username == "admin" && password == "test4321"
    #end
    if current_user.nil?
      flash[:error] = 'You must be signed in to view that page.'
      redirect_to new_session_path
    end
  end
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
end
