json.array!(@equipment) do |equipment|
  json.extract! equipment, :id, :name, :mark, :serial, :quantity, :brutto, :v_brutto, :comments
  json.url equipment_url(equipment, format: :json)
end
