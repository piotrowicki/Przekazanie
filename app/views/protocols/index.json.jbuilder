json.array!(@protocols) do |protocol|
  json.extract! protocol, :id, :date
  json.url protocol_url(protocol, format: :json)
end
