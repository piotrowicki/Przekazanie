class Equipment < ActiveRecord::Base
  belongs_to :protocol
  
  validates :name, :mark, :serial, :quantity, :brutto, :v_brutto,  :comments, :presence => true
  
   
end
