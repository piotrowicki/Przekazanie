class Protocol < ActiveRecord::Base
  has_many :equipments, :dependent => :destroy
  belongs_to :user
    
  
  belongs_to :user_to, :class_name => 'User', :foreign_key => 'user_to_id'
  belongs_to :company_to, :class_name => 'Company', :foreign_key => 'company_to_id'
  belongs_to :company, :class_name => 'Company', :foreign_key => 'company_id'

  validates :user_id, presence: true
    
 accepts_nested_attributes_for :equipments,  :allow_destroy => true
 
  
  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |protocol|
        csv << protocol.attributes.values_at(*column_names)
      end
    end
  end
end
