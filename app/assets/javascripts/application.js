// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap
//= require_tree .

var quantity_val;
var brutto_val;
var new_br;
var result ;

function doQuan(newid)
{
    //alert(newid.id);
    // Capture the entered values of two input boxes
    var quantity = document.getElementById(newid.id);
    quantity_val = quantity.value;

    var new_brutto_val = newid.id.toString(); //zamien id na string

    new_br = new_brutto_val.replace('quantity', 'v_brutto'); //zamien string na string v_brutto
    var v_brutto = document.getElementById(new_br); //znajdz element odpowiadajacy

    doMath(new_br);


}
function doBrut(newid)
{
    //alert(newid.id);
    // Capture the entered values of two input boxes
    var brutto = document.getElementById(newid.id);
    brutto_val = brutto.value;

    doMath(new_br);
    
 


}

function doMath(newid)
{
    result = parseFloat(quantity_val) * parseFloat(brutto_val);


    if (!isNaN(quantity_val) && !isNaN(brutto_val))
    {
        document.getElementById(newid).value = result.toFixed(2);
        if (isNaN(result) || result == null || result == "")
        {
            document.getElementById(newid).value = "";
        }
        console.log(result);
        // console.log(brutto_val);

    } else {
         document.getElementById(newid).value = "";
    }

}

  
